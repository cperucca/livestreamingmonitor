import fnmatch
import os
from shutil import copyfile

PARTENZA = '/home/perucccl/PRODUCTION/LiveStreamingMonitor'
ARRIVO = '/home/perucccl/LiveStreamingMonitorPROD'

matches = []

for root, dirnames, filenames in os.walk( PARTENZA ):
	for filename in fnmatch.filter(filenames, '*.py'):
		matches.append(os.path.join(root, filename))

for mat in matches :
	print 'copy : ' + mat + ' ' + mat.replace('perucccl/PRODUCTION/LiveStreamingMonitor', 'perucccl/LiveStreamingMonitorPROD')
	copyfile( mat , mat.replace('perucccl/PRODUCTION/LiveStreamingMonitor', 'perucccl/LiveStreamingMonitorPROD') )
