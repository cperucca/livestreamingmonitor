
# -*- coding: utf-8 -*-


import logging
import logging.config

import sys

import ast
import os
import urllib2, base64
from xml.dom import minidom
import xml.etree.ElementTree as ET
import json
from datetime import datetime, timedelta
import time
import copy
import requests

import ECE.Crea_List_Livestreaming as Crea_List_Livestreaming
import ECE.Prendi_Content_Da_ECE as Prendi_Content_Da_ECE
import FB.pyFacebookClass as pyFacebookClass
import YT.pyYoutubeClass as pyYoutubeClass
import WOW.pyWowzaClass as pyWowzaClass
import Resources.pySettaVars as pySettaVars
import APICoreX.API_DB as API_DB
import HTML.pyCreaHtml as pyCreaHtml

requests.packages.urllib3.disable_warnings()

# These two lines enable debugging at httplib level (requests->urllib3->http.client)
# You will see the REQUEST, including HEADERS and DATA, and RESPONSE with HEADERS but without DATA.
# The only thing missing will be the response.body which is not logged.
try:
    import http.client as http_client
except ImportError:
    # Python 2
    import httplib as http_client
http_client.HTTPConnection.debuglevel = 1
# You must initialize logging, otherwise you'll not see debug output.
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.setLevel(logging.DEBUG)
requests_log.propagate = True




def SettaEnvironment( debug ):

	if (debug):
		print 'PRIMA DI SETTARE ENVIRONMENT'
	if (debug):
		print 'ENV : '
	if (debug):
		print os.environ

	if (debug):
		print '---------------------------------'

	Environment_Variables = {}
	
	print 'verifico il setting della variabile _MonitorEnv_ '
	if '_MonitorEnv_' in os.environ:
		if 'PRODUCTION' in os.environ['_MonitorEnv_']:
			print 'variabile _MonitorEnv_ ha valore \'PRODUCTION\''
			print 'setto ENV di PROD'
			Environment_Variables = pySettaVars.PROD_Environment_Variables
			pySettaVars.LOGS_CONFIG_FILE = pySettaVars.PROD_CONFIG_FILE
			pySettaVars.WOW_SERVER = pySettaVars.PROD_WOW_SERVER
		else:
			print 'variabile _MonitorEnv_ ha valore : ' + os.environ['_MonitorEnv_']
			print 'diverso da \'PRODUCTION\''
			print 'setto ENV di TEST'
			Environment_Variables = pySettaVars.TEST_Environment_Variables
			pySettaVars.LOGS_CONFIG_FILE = pySettaVars.TEST_CONFIG_FILE
			pySettaVars.WOW_SERVER = pySettaVars.TEST_WOW_SERVER
			
	else:
		print 'variabile _MonitorEnv_ non trovata: setto ENV di TEST'
		Environment_Variables = pySettaVars.TEST_Environment_Variables
		pySettaVars.LOGS_CONFIG_FILE = pySettaVars.TEST_CONFIG_FILE
		pySettaVars.WOW_SERVER = pySettaVars.TEST_WOW_SERVER


	for param in Environment_Variables.keys():
		#print "%20s %s" % (param,dict_env[param])
		os.environ[param] = Environment_Variables[ param ]



	# metto il valore del WOW Server anche nell environment
	os.environ['WOW_SERVER'] = pySettaVars.WOW_SERVER

	print ' settato WOW = ' + pySettaVars.WOW_SERVER
	print ' settato ECE = ' + os.environ['ECE_SERVER']
	print ' settato ID_MONITOR = ' + os.environ['ID_MONITOR']

	if (debug):
		print 'DOPO AVER SETTATO ENVIRONMENT'
	if (debug):
		print 'ENV : '
	if (debug):
		print os.environ


def Prendi_Id_StreamName( VideoDetailsDictionary ):
	result = None
	id = None

	if 'LivestreamsDetails' in VideoDetailsDictionary and 'cdn' in VideoDetailsDictionary['LivestreamsDetails'] and  \
		'ingestionInfo' in VideoDetailsDictionary['LivestreamsDetails']['cdn'] and 'streamName' in VideoDetailsDictionary['LivestreamsDetails']['cdn']['ingestionInfo']:
		logger.debug('streamname = ' + VideoDetailsDictionary['LivestreamsDetails']['cdn']['ingestionInfo']['streamName'])
		result = VideoDetailsDictionary['LivestreamsDetails']['cdn']['ingestionInfo']['streamName']

	if 'BindDetails' in VideoDetailsDictionary and 'id' in VideoDetailsDictionary['BindDetails']:
		logger.debug('bind Id = ' + VideoDetailsDictionary['BindDetails']['id'])
		id = VideoDetailsDictionary['BindDetails']['id']

	return [ result , id ]

def GetActiveArticlesLive( db_name , active_dict):

	print ' ----INIT----- GetActiveArticlesLive : ' + db_name

	try:
		lista_db = API_DB.Prendi_Items_Db( db_name )
		active_dict.update(lista_db)

	except Exception as e:
		print 'PROBLEMI CON IL DB in GetActiveArticlesLive: ' + str(e)
		pass
	
	print ' ----END----- GetActiveArticlesLive : ' + db_name
	return active_dict
	

def GetActiveArticles( db_name ):

	print ' ----INIT----- GetActiveArticles : ' + db_name
	active_dict = {}

	try:
		lista_db = API_DB.Prendi_Items_Db( db_name )
		[ da_fare_list_PREPARA, da_fare_list_ON, da_fare_list_OFF, da_fare_list_NOTIFICA ] = Prendi_Content_Da_ECE.Controlla_Flag_Social( lista_db )

		active_dict.update( da_fare_list_PREPARA )
		active_dict.update( da_fare_list_ON )	
		active_dict.update( da_fare_list_NOTIFICA )	
	

		#print lista_db

		#for key,value in lista_db.iteritems():
			#print value['SocialFlags']
			#for keysoc,valuesoc in value['SocialFlags'].iteritems():
				#print keysoc, valuesoc

	except Exception as e:
		print 'PROBLEMI CON IL DB in GetActiveArticles: ' + str(e)
		pass
	
	print ' ----END----- GetActiveArticles : ' + db_name
	return active_dict
	
def PulisciListaStreamTargets( lista_stream_targets, logger ):
	logger.debug(' -----INIT----- PulisciListaStreamTargets ')
	result = {}
	for key,value in lista_stream_targets.iteritems():
		logger.debug( ' sessionStatus = ' + value['sessionStatus'] )
		logger.debug( key )
		logger.debug( ' enabled = ' + str(value['enabled'] ))
		if  not 'NotFound' in value['sessionStatus'] or value['enabled']:
			result[ key ] = value
		else:
			if 'yt-account' in key  and 'NotFound' in value['sessionStatus']:
				result[ key ] = value

	logger.debug(' result = ' + str( result ))
	logger.debug(' -----END----- PulisciListaStreamTargets ')
	return result

def GetVideoList( lista_stream_targets ):

	print lista_stream_targets
	logger.debug(" ---------------------- INIT GetVideoList  ------------------ " )
	
	for key,value in lista_stream_targets.iteritems():
		tmpkey = key.split('_post:')
		tmpkey = tmpkey[-1].split('_account:')
		print tmpkey[0], tmpkey[-1]
		live_details = {}
		if 'yt-account' in tmpkey[-1] :
			# sistemato come segue quando abbiamo aggiunto YT 
			# per non cambiare le procedure a valle 
			# poiche YT mi ritorna direttamente -ready- mentre FB mi ritorna un json
			# trasformo il ready in json come segue
			print 'siamo qui'
			live_details = {'status' : pyYoutubeClass.Get_LiveBroadcast_Status( tmpkey[0], tmpkey[-1] ),
					'embed_html' :  pyYoutubeClass.Get_LiveBroadcast_Content_Details( tmpkey[0], tmpkey[-1] ) } 
		else:
			live_details =  pyFacebookClass.Get_Live_Video_Details( tmpkey[0], tmpkey[-1] )

		
		print ' live_details : ' + str(live_details)
		
		

		if 'error' in live_details:
			logger.debug( ' Trovato error nel live_details del post')
			lista_stream_targets[ key ]['post_details'] = { 'status' : 'ERROR', 'embed_html': live_details }
		else:
			logger.debug( ' NON Trovato error nel live_details del post')
			lista_stream_targets[ key ]['post_details'] = live_details
			

	logger.debug(" ---------------------- FINE GetVideoList  ------------------ " )
	return lista_stream_targets

def Manda_Errori( lista_errori ):

	logger.debug(" ---------------------- DEBUG INIT Manda_Errori  ------------------ ")
	jsonArrayDaMandare = []

	print os.environ['ERROR_REST']
	rest_errori =  os.environ['ERROR_REST']
	rest_user = os.environ['ERROR_USER']
	rest_pwd =  os.environ['ERROR_PWD']

	print lista_errori
	# qui ho roba cosi':  {'LS_11465421_031': [True, 'NON arriva enc8 al Wowza : controllare il Wirecast', 'Non esistono i Dettagli del post', ''], 'LS_11465421_030': [True, '', 'NON CI sono i VideoDetails', '']}
	# che deve diventare cosi [ { “liveSocialId”: “XXXXX”, “status”: true, “errorMessage”: “YYYYY”}]

	error = { 'NOTIFY' : 0,'WOW_IN':1,'WOW_OUT':2, 'SOCIAL':3 }
	# quindi :
	for key,value in lista_errori.iteritems():
		#print key, value

		result = {}
		result['liveSocialId'] = key
		result['status'] = True
		result['errorMessage'] = ''
		for val in range( 3, 0, -1 ):
			#print str(val) + ' ------------- ' + value[val]
			if value[ error['NOTIFY']] and len(value[val])> 0 :
				print 'val maggiore di 0'
				result['errorMessage'] = value[val]
				result['status'] = False
		
		jsonArrayDaMandare.append( result )

	logger.debug( jsonArrayDaMandare )
		
	try:


		meta =  jsonArrayDaMandare
		datas = json.dumps(meta)
		print datas

		param = { }

		base64string = base64.encodestring('%s:%s' % (rest_user, rest_pwd)).replace('\n', '')
		headers =  {'Authorization': 'Basic ' + base64string,
			   'Content-type': 'application/json'}

		return_req = requests.request('POST', rest_errori,headers=headers,data=datas)
		print return_req
		
		result = json.loads(return_req.content)            
		#logger.debug(" ---------------------- DEBUG Update_LiveBroadcast  ------------------ ")
		logger.debug(result)
		logger.debug(" ---------------------- DEBUG END Manda_Errori  ------------------ ")
		
	except:
		pass


	

if __name__ == "__main__":

	# questa e per settare le veriabili di ambiente altrimenti come cron non andiamo da nessuna parte
	SettaEnvironment( False )

	print '\n\n log configuration file : ' +  pySettaVars.LOGS_CONFIG_FILE + '\n'
	logging.config.fileConfig(pySettaVars.LOGS_CONFIG_FILE)
	logger = logging.getLogger('pyMonitor')
	print ' logging file : \t\t' + logger.handlers[0].baseFilename + '\n'

	
	'''
	os.environ['WOW_RESOURCE_DIR'] = '/home/perucccl/APICoreX/WOW/RESOURCES'
	os.environ['WOW_DIGEST_USER'] = 'perucccl'
	os.environ['WOW_DIGEST_PWD'] = 'perucccl'
	os.environ['WOW_APPLICATION'] = 'multimedia_live'
	os.environ['WOW_SERVER'] = 'rsis-tifone-01'
	os.environ['DB_NAME'] = '/home/perucccl/STAGING/APICoreX/Resources/_LivestreamingDb_'
	os.environ['ECE_SERVER'] = 'http://internal.publishing.staging.rsi.ch/webservice/escenic/content/'

	'''

	#print os.environ
	#exit(0)
	logger.info(' LiveStreamingMonitor : ' + sys.argv[0] )  

	logger.debug( 'prendo pyWowzaClass.ListaIncomingStreams sul server : ' + os.environ['WOW_SERVER'] )
	lista_incoming_streams = pyWowzaClass.ListaIncomingStreams( os.environ['WOW_SERVER'] , 'multimedia_live')
	logger.debug( 'prendo pyWowzaClass.ListaStreamTargets sul server : ' + os.environ['WOW_SERVER'] )
	lista_stream_targets = pyWowzaClass.ListaStreamTargets( os.environ['WOW_SERVER'] , 'multimedia_live')
	logger.debug( lista_stream_targets )

	
	logger.debug( 'pulisco ListaStreamTargets ')
	lista_stream_targets = PulisciListaStreamTargets( lista_stream_targets, logger )

	logger.debug( 'prendo GetActiveArticles sul DB : ' +  os.environ['DB_NAME']  )
	# questa chiama la Controlla_Flag_Social e ne mette insieme le liste
	lista_active_articles = GetActiveArticles( os.environ['DB_NAME'] )
	# mentre questa va a guardare il DB del LiveCentre
	lista_active_articles = GetActiveArticlesLive( os.environ['DB_LIVECENTRE'], lista_active_articles )
	
	logger.debug( 'prendo pyFacebookClass.GetVideoList  per la lista : ' +  str(lista_stream_targets ))
	lista_targets_fb = GetVideoList( lista_stream_targets )
	print lista_targets_fb

	lista_accese = {}
	[ html_body, lista_accese, lista_errori ] = pyCreaHtml.CreaEventHtml( lista_incoming_streams, lista_targets_fb, lista_active_articles )
	logger.debug('lista PER DB : ' + str( lista_accese))
	
	lista_da_db = API_DB.Prendi_Items_Db( os.environ['DB_MONITOR'])
	logger.debug( 'lista DA DB : ' + str(lista_da_db ))

	logger.debug( 'lista ERRORI : ' + str(lista_errori ))
	
	logger.debug( 'DB del monitor : ' + os.environ['DB_MONITOR'])

	if not ( lista_da_db == lista_accese ) :
		# devo fare update del codewidget 
		# perche' il DB e' cambiato
		if '_MonitorEnv_' in os.environ and 'PRODUCTION' in os.environ['_MonitorEnv_']:
			id_monitor = '10378235'
			os.environ['ECE_SERVER'] = 'http://internal.publishing.production.rsi.ch/webservice/escenic/content/'
			logger.debug('setto id del code widget di produzione = 10378235')
		else:
			logger.debug(' sono in TEST')
			id_monitor = '10378235'
			os.environ['ECE_SERVER'] = 'http://internal.publishing.staging.rsi.ch/webservice/escenic/content/'
			logger.debug('setto id del code widget di staging = 10378235 ')

		logger.debug('Modifico Code Widget = ' + id_monitor)
		Prendi_Content_Da_ECE.UpdateMonitorProd( id_monitor, html_body )
		logger.debug('mando eventuali errori a Giuliano ')
		Manda_Errori( lista_errori )
	else:
		if '_MonitorEnv_' in os.environ and 'PRODUCTION' in os.environ['_MonitorEnv_']:
			logger.debug( '\nlista_accese uguale a quella del DB : non faccio update del code widget')
			logger.debug( '\nlista_accese uguale a quella del DB : e non mando errori a Giuliano')
		else:
			id_monitor = '10378235'
			logger.debug( '\nlista_accese uguale a quella del DB : ')
			logger.debug('Modifico lo stesso Code Widget = ' + id_monitor)
			logger.debug('Perche sono in TEST')
			Prendi_Content_Da_ECE.UpdateMonitor( id_monitor, html_body )
			logger.debug('e mando lo stesso eventuali errori a Giuliano ')
			logger.debug('Perche sono in TEST')
			Manda_Errori( lista_errori )
			

	API_DB.Scrivi_Items_Db( os.environ['DB_MONITOR'], lista_accese )




