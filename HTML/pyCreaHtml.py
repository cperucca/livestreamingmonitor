

# -*- coding: utf-8 -*-
import os
import logging
import logging.config
import re
from datetime import datetime, timedelta

import Resources.html_table_template as html_table_template

logger = logging.getLogger('pyHtml')

encoders = [ 'dummy', 'LA1', 'LA2', 'ENC 14', 'ENC 15', 'ENC 20a', 'ENC 20b','ENC 21a','Social_Only','Intrastream' ]

template_iniziale = '<td title="__TITLE__" id="__CELL__r__CELLROW__c__CELLCOL__">__ENCODER__&nbsp;</td>\n'
template_mezzo = '<td title="__TITLE__" id="cell_r__CELLROW__c__CELLCOL__" style="background:__COLORE__; color:__COLORE__;">__VALUE__&nbsp;</td>\n'
template_finale = '<td >&nbsp;</td> \n'

col_event_template = ' <tr> \n <td title="__TITOLO_OVER__" id="cell_r__CELLROW__c01" >__ECE_ID__&nbsp;</td> \n <td >&nbsp;</td>  \n <td title="" id="cell_r__CELLROW__c02" >__START_T__&nbsp;</td> \n <td title="" id="cell_r__CELLROW__c03" >__END_T__&nbsp;</td> \n <td >&nbsp;</td> \n <td title="" id="cell_r__CELLROW__c04" >__NOTIFICA_T__&nbsp;</td> \n <td >&nbsp;</td> \n <td title="" id="cell_r__CELLROW__c05" >__ENCODER__&nbsp;</td> \n <td >&nbsp;</td> \n <td title="__LIVE_ID_OVER__" id="cell_r__CELLROW__c06" >__FB_CHANNEL__&nbsp;</td> \n <td >&nbsp;</td> \n <td title="" id="cell_r__CELLROW__c07" >__WOW_IN__&nbsp;</td> \n <td title="" id="cell_r__CELLROW__c08" __WOW_DASH_PLAYER__ &nbsp;</td> \n <td >&nbsp;</td> \n <td title="" id="cell_r__CELLROW__c09" >__WOW_OUT__&nbsp;</td> \n <td >&nbsp;</td>  \n <td title="" id="cell_r__CELLROW__c10" >__FB_POST__&nbsp;</td> \n <td title="" id="cell_r01c11" __FB_DASH_PLAYER__ &nbsp;</td> \n <td >&nbsp;</td> \n <td title="" id="cell_r__CELLROW__c12" style="width:160px;height:90px;" >__LIVE__&nbsp;</td> \n </tr> \n ' 

fb_dash_player_template = 'style="width:160px;height:90px;" ><div> <video muted data-dashjs-player autoplay controls src="__FB_DASH_URL__" > </video> </div>' 

wow_dash_player_template = 'style="width:160;height:90px;"><div> <video muted data-dashjs-player autoplay controls src="__WOW_DASH_URL__" > </video> </div> '


colori = { 'normale' : '#333333' , 'rosso' : '#ff7d7d', 'verde' : '#c6e1b4', 'giallo' : '#ffe17d', 'fb' : '#4267b2', 'yt' : '#ac362e'}

#template_finale = '<td id="__CELL__r__CELLROW__c__CELLCOL__">__ENCODER__&nbsp;</td>\n <td id="cell_r__CELLROW__c__CELLCOLPIUUNO__">&nbsp;</td>\n'

col_mapping = { 'server' : '02', 
		'fb_account_rsi_news_stream' : '03', 'fb_account_rsi_news_post' : '04', 
		'fb_account_rsi_sport_stream' : '05', 'fb_account_rsi_sport_post' : '06', 
		'fb_account_rsi_stream' : '07', 'fb_account_rsi_post' : '08', 
		'fb_account_testare_e_bello_stream' : '09', 'fb_account_testare_e_bello_post' : '10', 
		'fb_account_linea_rossa_diretta_stream' : '11', 'fb_account_linea_rossa_diretta_post' : '12' 
}

enc_mapping = { 'la1.stream' : '01',
		'la2.stream' : '02',
		'enc14': '03',
		'enc15': '04',
		'enc20a': '05',
		'enc20b': '06',
		'enc21a': '07',
		'social_only': '08',
		'intrastream': '09'
}

# questa serve per dare il nome "giusto" alle varie colonne
# quando poi riempiamo le liste dei colori ( per esempio )
event_map = { 'ece_id' : 'c01',
		'start_t' : 'c02',
		'end_t': 'c03',
		'notifica_t': 'c04',
		'encoder': 'c05',
		'FB_channel': 'c06',
		'wow_in': 'c07',
		'win_preview': 'c08',
		'wow_out': 'c09',
		'FB_post': 'c10',
		'fb_preview': 'c11',
		'live': 'c12'
}




def mc( row_str, col_str ):

	try:
		# prende le celle dal nome e le mette in numeri
		result = ''
		result = 'cell_r' + enc_mapping[ row_str ] + 'c' + col_mapping[ col_str ]
		return result
	except:
		print 'Errore mapping'
		pass

	return ''
		
def CreaBody( lista_accese ):

	result = ''

	# al massimo faccio 10 righe ?
	for row in range( 1, 10 ):
		#print encoders[ row ]
		result = result + '<tr>\n'
		#print '	<tr>'
		count = 1
		for col in range( 1, 18 ):
			
			#print str(row).zfill(2),str(count).zfill(2)
			rowid = str(row).zfill(2) 
			colid = str(count).zfill(2)
			tmp_id = 'cell_r' + rowid + 'c' + colid

			if col == 1:
				template = template_iniziale
			else:
				if col%3 == 0:
					template = template_finale
					count -= 1
				else:
					template = template_mezzo

			#rep = { '__CELLROW__': rowid,  '__CELLCOLPIUUNO__': colid_piuuno, '__CELLCOL__': colid }
			rep = { '__CELLROW__': rowid,  '__CELLCOL__': colid, '__TITLE__' : '' }

			if col == 1:
				rep['__ENCODER__'] =  encoders[row] 
				rep['__CELL__'] =  'enc'
			else:
				rep['__ENCODER__'] =  ''
				rep['__CELL__'] =  'cell_'


			if tmp_id in lista_accese:
				rep[ '__COLORE__' ] = colori[lista_accese[ tmp_id ]['colore']]
				rep[ '__TITLE__' ] = lista_accese[ tmp_id ]['valore']
				rep[ '__VALUE__' ] = lista_accese[ tmp_id ]['valore']
			else:
				rep[ '__COLORE__' ] = colori['normale']
				rep[ '__VALUE__' ] = ''

			# use these three lines to do the replacement
			rep = dict((re.escape(k), v) for k, v in rep.iteritems())
			pattern = re.compile("|".join(rep.keys()))
			tmp_template = pattern.sub(lambda m: rep[re.escape(m.group(0))], template)

			#print tmp_template
			result = result + tmp_template
			count += 1
			
		result = result + '</tr>\n'
		#print '	</tr>'

	return result
	


def FormatTime( time ):

	#logger.debug( 'formatto il time: ' + str(time)) 

	#logger.debug( 'formatto il time: ' + str(datetime.strptime( time,  '%Y-%m-%dT%H:%M:%SZ' )))
	f_time = datetime.strptime( time ,  '%Y-%m-%dT%H:%M:%SZ' )
	f_time = f_time + timedelta(minutes = 1)
	#logger.debug( ' formattato : ' + f_time.strftime('%H:%M del %d-%m-%y '))
	return f_time.strftime('%H:%M <br> %d-%m-%y ')
	


		

def PassatoTime( notificatime ):

	# cosi considero anche eventuali cambiamenti di daysaving time 
	adesso = datetime.now()
	# per controllo su data emissione mi server il tempo attuale
	#print 'prendo tempo attuale in utc ' + str(adesso)

	if adesso > datetime.strptime( notificatime,  '%Y-%m-%dT%H:%M:%SZ' ):
		return True

	return False

def Get_Embed_Live( post_details, plat ):

	return Get_Embed( post_details, plat, False)
	
def Get_Embed_Preview( post_details, plat ):

	return Get_Embed( post_details, plat, True)
	

def Get_Embed( post_details, plat, preview ):

	# uso la stessa funzione per il live ed il preview
	# che vedo solo aggiungere livemonitor=1 alla url
	# per YT

	result = 'ERROR'
	# in post_details ho  roba tipo:
	'''
	{
		u'items': [{
			u'contentDetails': {
				u'closedCaptionsType': u'closedCaptionsDisabled',
				u'projection': u'rectangular',
				u'startWithSlate': False,
				u'boundStreamId': u'FaiGHpzJN453puqlWvZnPg1558945782659593',
				u'enableAutoStart': False,
				u'latencyPreference': u'normal',
				u'enableEmbed': True,
				u'enableClosedCaptions': False,
				u'enableLowLatency': False,
				u'boundStreamLastUpdateTimeMs': u'2019-05-27T08: 29: 42.675Z',
				u'enableContentEncryption': False,
				u'recordFromStart': True,
				u'enableDvr': True,
				u'monitorStream': {
					u'broadcastStreamDelayMs': 0,
					u'embedHtml': u'<iframe width="425"height="344"src="https://www.youtube.com/embed/OiUZQ0lylrQ?autoplay=1&livemonitor=1"frameborder="0"allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"allowfullscreen></iframe>',
					u'enableMonitorStream': True
				}
			},
			u'kind': u'youtube#liveBroadcast',
			u'etag': u'"XpPGQXPnxQJhLgs6enD_n8JR4Qk/Dq625WHOeFhjIMtr8J1dhGLtSEY"',
			u'id': u'OiUZQ0lylrQ'
		}],
		u'kind': u'youtube#liveBroadcastListResponse',
		u'etag': u'"XpPGQXPnxQJhLgs6enD_n8JR4Qk/YR-nSucfVQilA5W96GSz8SkXaHs"',
		u'pageInfo': {
			u'resultsPerPage': 5,
			u'totalResults': 0
			}
	}
	per YT e 
		'<iframesrc="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2F1237091532989230%2Fvideos%2F410780876178098%2F&width=400"width="400"height="400"style="border:none;overflow:hidden"scrolling="no"frameborder="0"allowTransparency="true"allowFullScreen="true"></iframe>',

	per FB

	'''
	
	# da cui devo prendere il valore di embed html e metterlo nella forma giusta per 
	# il mio code widget
	# cioe <iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2F1237091532989230%2Fvideos%2F1872635006101543%2F&width=300" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="false"></iframe>

	if 'fb' in plat :
		# allora se entra FB faccio
		fb_live =  post_details

		tail = '&width=300" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="false"></iframe>'
		result =  fb_live.split('&width')[0] + tail
		logger.debug( 'fb_live = ' + result )
	else:
		# sono in YT e faccio :
		if 'items' in post_details and len(post_details['items'] ) > 0:
			fb_live = post_details['items'][0]
			if 'contentDetails' in fb_live and 'monitorStream' in fb_live['contentDetails'] and 'embedHtml' in fb_live['contentDetails']['monitorStream']:
				# ci sono tutti i pezzi : prendo la urn
				template = '<iframe width="300" src="__REPLACE_SRC__?autoplay=1" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="false"></iframe>'
				template_preview = '<iframe width="300" src="__REPLACE_SRC__?autoplay=1&livemonitor=1" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="false"></iframe>'
				fb_live = fb_live['contentDetails']['monitorStream']['embedHtml']
				fb_live =  fb_live.split('?autoplay')[0] 
				fb_live =  fb_live.split('src="')[-1] 
				print fb_live
			
				if len( fb_live) > 0 :
					if ( preview ) :
						result = template_preview.replace('__REPLACE_SRC__',fb_live )
					else:
						result = template.replace('__REPLACE_SRC__',fb_live )
				print result
				
				# e in fb_live ho <iframe width="425"height="344"src="https://www.youtube.com/embed/OiUZQ0lylrQ?autoplay=1&livemonitor=1"frameborder="0"allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"allowfullscreen></iframe>',
				# da cui devo prendere la url
			else:
				# manca la url devo segnare errore ?
				return 'ERROR'




	return result


		
def CreaEventBody( lista_incoming_streams, lista_stream_targets, lista_active_articles ):

	logger.info( '------------ INIT ---------- CLAD LiveStreamingMonitor/CreaEventBody ---------------------')
	result = ''
	lista_id_ece = {}
	lista_gialli = []
	lista_rossi = []
	lista_verdi = []
	lista_fb = []
	lista_yt = []
	lista_errori = {}
	

	error = { 'NOTIFY' : 0,'WOW_IN':1,'WOW_OUT':2, 'SOCIAL':3 }
	rowcount = 1
	for article in lista_active_articles:
		art = article[0]
		value = article[1]
		logger.info( ' art -> ' + art )
		logger.info( ' value : ' + str(value ))
		if 'SocialFlags' in value:
			#logger.debug( value['SocialFlags'] )
			_soc = value['SocialFlags']
			# e adesso girando sugli eventuali WowzaDetail e VideoDdetails
			# degli FBStreamingDetails per ogni account che si trova nella
			# lista fb-livestreaming-account possiamo colorare le celle giuste
			# cambiando opportunamente la lista di rep da passare a pattern.sub
			logger.debug( ' giro sui social accounts ' )

			#for account in _soc['fb-livestreaming-account']:

			platforms = ['fb' , 'yt' ]
			for plat in platforms:
				account = _soc[ plat + '-livestreaming-account' ]
				for key,valueUp in account.iteritems():
					logger.debug( ' key : ' + key )
					logger.debug( ' valueUp : ' + str( valueUp ))
					# print key, valueUp
					valueSoc = valueUp['SocialDetails']
					if 'VideoDetails' in valueUp:
						valueVideo = valueUp['VideoDetails']
					if 'WowzaDetails' in valueUp:
						valueWow = valueUp['WowzaDetails']

					lista_errori[ valueSoc['livesocialid'] ] = [ False, "", "", "" ]

					rep = {}

					# inizializzo indice di riga
					rowid = str(rowcount).zfill(2) 
					rep['__CELLROW__'] = rowid
					rowcount += 1

					lista_canale = lista_fb if 'fb' in plat else lista_yt
					lista_canale.append( 'id="cell_r' + rowid + event_map['ece_id'] + '"' )

					print lista_canale

					logger.debug( _soc['title'] )

					rep['__ECE_ID__'] = art
					rep['__TITOLO_OVER__'] = _soc['title'].replace('"','')   
					rep['__START_T__'] = FormatTime( _soc['t_start'] )
					rep['__END_T__'] = FormatTime(_soc['t_end'])
					logger.debug( _soc['t_end'] )
					# mentre invece il tempo di notifica non e piu generale ma 
					# di ogni singolo account
					rep['__NOTIFICA_T__'] = FormatTime(valueSoc['notificaTime'])
					logger.debug( valueSoc['notificaTime'] ) 
					rep['__ENCODER__'] =  valueSoc['incoming_stream']
					logger.debug( valueSoc['incoming_stream'] )

					# e adesso verifico che il _soc['incoming_stream'] sia gia' attaccato al server
					# wowza cioe' che si trovi nella lista_incoming_streams

					#logger.debug( ' lista_incoming_streams = ' + str(lista_incoming_streams)) )
					#logger.debug( ' lista_incoming_streams starting point = ' + str(lista_incoming_streams[_soc['incoming_stream']])) )

					if valueSoc['incoming_stream'] in lista_incoming_streams:
						logger.debug( 'WOW_IN deve diventare VERDE' )
						lista_verdi.append( 'id="cell_r' + rowid + event_map['wow_in'] + '"' )
						rep['__WOW_IN__'] =  str(lista_incoming_streams[valueSoc['incoming_stream']])
						wow_tmp_dash_url = 'http://' + os.environ['WOW_SERVER'] + ':1935/multimedia_live/' +  valueSoc['incoming_stream'] + '_monitor/manifest.mpd'.strip()
						
						rep['__WOW_DASH_PLAYER__'] = wow_dash_player_template.replace('__WOW_DASH_URL__',wow_tmp_dash_url)
					else:
						logger.debug( 'WOW_IN deve diventare ROSSO' )
						lista_rossi.append( 'id="cell_r' + rowid + event_map['wow_in'] + '"' )
						
						rep['__WOW_IN__'] = 'NON arriva ' + valueSoc['incoming_stream'] + ' al Wowza : controllare il Wirecast'
						lista_errori[ valueSoc['livesocialid'] ][ error['WOW_IN'] ] = rep['__WOW_IN__']
						rep['__WOW_DASH_PLAYER__'] = '>NON arriva ancora ...'

					# non piu accont semplicemente come prima ma adesso devo prendere il valore 
					# sotto gli appropriati SocialDetails
					# rep['__FB_CHANNEL__'] = account
					rep['__FB_CHANNEL__'] = valueSoc['livestreamingaccount']
					rep['__LIVE_ID_OVER__'] = valueSoc['livesocialid']

					# e coloro i tempi in base al tempo attuale
					if PassatoTime(valueSoc['notificaTime']):
						logger.debug( 'PASSATA NOTIFICA' )
						# metto la casella della notifica in verde per far vedere che e'  ok
						lista_verdi.append( 'id="cell_r' + rowid + event_map['notifica_t'] + '"' )
						lista_errori[ valueSoc['livesocialid'] ][ error['NOTIFY'] ] = True
					else:
						logger.debug( 'NON PASSATA NOTIFICA' )
						rep['__FB_POST__'] = 'NON ancora notificato'
						rep['__LIVE__'] = 'NON ancora notificato'
						rep['__WOW_OUT__'] = 'NON ancora notificato'
						rep['__FB_DASH_PLAYER__'] = '>NON ancora notificato'

						# inserisco il rep per la lista che finisce nel db
						if '__LIVE_ID_OVER__' in rep:
							logger.debug( ' Inserisco nella lista lista_id_ece')
							lista_id_ece[ rep['__LIVE_ID_OVER__'] ] = rep
						else:
							logger.debug( ' NON Inserisco nella lista lista_id_ece')

						# use these three lines to do the replacement
						rep = dict((re.escape(k), v) for k, v in rep.iteritems())
						pattern = re.compile("|".join(rep.keys()))
						tmp_template = pattern.sub(lambda m: rep[re.escape(m.group(0))], col_event_template)
						result = result + tmp_template
						continue
						
					if PassatoTime(_soc['t_start']):
						# metto la casella della notifica in verde per far vedere che e'  ok
						lista_verdi.append( 'id="cell_r' + rowid + event_map['start_t'] + '"' )
					if PassatoTime(_soc['t_end']):
						# metto la casella della notifica in verde per far vedere che e'  ok
						lista_verdi.append( 'id="cell_r' + rowid + event_map['end_t'] + '"' )
					


					fb_status = 'ERROR'

					if 'WowzaDetails' in valueUp:
						# e quelli dei WowzaDetails
						logger.debug( ' Verifico i WowzaDetails ')
						logger.debug( valueWow )

						# CLAD -> devo andare a verificare il wowzastatus
						# prendendolo dalla ListaStreamTargets
						
						wow_status = ''
						logger.debug( 'lista_stream_targets = ' + str(lista_stream_targets ))
						logger.debug( ' entryNAme = ' + valueWow['entryName'] )

						if valueWow['entryName'] in lista_stream_targets:
							logger.debug(  'entry in lista_streamtargets ' )
							stream_target = lista_stream_targets[valueWow['entryName']] 
							wow_status = stream_target['sessionStatus']
						else:
							logger.debug(  ' WowzaDetails entry NON in lista_streamtargets ' )
							wow_status = 'NON esiste nella lista_streamtargets'

						logger.debug(  'WOW STATUS = ' + wow_status )


						logger.debug(' CLAD ->>>>>>>>>>>> ' + valueWow['entryName'] + ' ------ ' + wow_status )

						if 'Active' in wow_status or ( 'yt-account' in  valueWow['entryName'] and 'NotFound' in wow_status ):
							# in base allo wow_status posso decidere il colore
							logger.debug( 'ACTIVE ' )
							lista_verdi.append( 'id="cell_r' + rowid + event_map['wow_out'] + '"' )
							rep['__WOW_OUT__'] = 'StreamTarget in Wowza Attivo'
						else:
							if 'Error' in wow_status:
								logger.debug( 'wow  di Error ' )
								lista_rossi.append( 'id="cell_r' + rowid + event_map['wow_out'] + '"' )
								rep['__WOW_OUT__'] = 'StreamTarget in Wowza in Errore'
								lista_errori[ valueSoc['livesocialid'] ][ error['WOW_OUT'] ] = rep['__WOW_OUT__']
							else:
								if 'Not Found' in wow_status:
									logger.debug( 'wow else di Error  ' )
									lista_gialli.append( 'id="cell_r' + rowid + event_map['wow_out'] + '"' )
									rep['__WOW_OUT__'] = 'StreamTarget in Wowza Waiting'
								else:
									if 'NON esiste nella lista_streamtargets' in wow_status:
										logger.debug( 'non eiste in lista_streams ' )
										lista_rossi.append( 'id="cell_r' + rowid + event_map['wow_out'] + '"' )
										rep['__WOW_OUT__'] = 'NON esiste nella lista_streamtargets'
										lista_errori[ valueSoc['livesocialid'] ][ error['WOW_OUT'] ] = rep['__WOW_OUT__']
									else:
										lista_rossi.append( 'id="cell_r' + rowid + event_map['wow_out'] + '"' )
										rep['__WOW_OUT__'] = 'Errore sconosciuto'
										lista_errori[ valueSoc['livesocialid'] ][ error['WOW_OUT'] ] = rep['__WOW_OUT__']
									
										
								
	
					else:
						# non ci sono i WowzaDetails e quindi non e' stato ancora fatto
						# il post di notifica
						logger.debug( 'non ci sono i WowzaDetails ')
						lista_rossi.append( 'id="cell_r' + rowid + event_map['wow_out'] + '"' )
						rep['__WOW_OUT__'] = 'Non ci sono i WowzaDetails'
						lista_errori[ valueSoc['livesocialid'] ][ error['WOW_OUT'] ] = rep['__WOW_OUT__']




					if 'VideoDetails' in valueUp:
						# quindi posso prendere i valori dei VideoDetails
						logger.debug( 'CI sono i VideoDetails ')
						logger.debug( 'valueVideo = ' + str( valueUp['VideoDetails']   ))
						logger.debug( 'lista_stream_targets = ' + str( lista_stream_targets   ))


						if valueVideo['wow_name'] in lista_stream_targets:
							logger.debug(  'entry in lista_streamtargets ' )
							post_details = lista_stream_targets[valueVideo['wow_name']]['post_details'] 
							fb_post_status = post_details['status']

							logger.debug( 'fb_post_status = ' + str( fb_post_status))

							if 'ERROR' in fb_post_status:


								fb_live = post_details['embed_html']
								fb_post_message = post_details['embed_html']['error']['message']

								# e qui devo mettere il valore del live in live
								rep['__LIVE__'] = 'NON ESISTE'
								rep['__FB_DASH_PLAYER__'] = '>NON ESISTE'
								rep['__FB_POST__'] = fb_post_message
								lista_rossi.append( 'id="cell_r' + rowid +  event_map['FB_post'] +'"' )
								lista_rossi.append( 'id="cell_r' + rowid + event_map['live'] + '"' )
								lista_errori[ valueSoc['livesocialid'] ][ error['WOW_OUT'] ] = rep['__FB_POST__']
							else:

								fb_live = Get_Embed_Live( post_details['embed_html'] , plat )
								fb_preview_url = 'style="width:160px;height:90px;" >' + Get_Embed_Preview( post_details['embed_html'] , plat ) if 'yt' in plat else fb_dash_player_template.replace('__FB_DASH_URL__', post_details['dash_preview_url'])
			
								rep['__LIVE__'] = fb_live
								rep['__FB_POST__'] = fb_post_status
								rep['__FB_DASH_PLAYER__'] = fb_preview_url

								logger.debug( 'FB_POST c09 e _LIVE_ c10 devono diventare verdi' )
								lista_verdi.append( 'id="cell_r' + rowid + event_map['FB_post'] + '"' )
								lista_verdi.append( 'id="cell_r' + rowid + event_map['live'] + '"' )
							
						else:
							logger.debug(  'VideoDetails entry NON in lista_streamtargets ' )
							lista_rossi.append( 'id="cell_r' + rowid + event_map['wow_out'] + '"' )
							lista_rossi.append( 'id="cell_r' + rowid +  event_map['FB_post'] +'"' )
							lista_rossi.append( 'id="cell_r' + rowid + event_map['live'] + '"' )
							rep['__FB_POST__'] = 'NON CI sono i VideoDetails'
							rep['__LIVE__'] = 'Non Esiste'
							rep['__FB_DASH_PLAYER__'] = '>NON ESISTE'
							#rep['__WOW_OUT__'] = 'entry NON in lista_streamtargets'
							lista_errori[ valueSoc['livesocialid'] ][ error['WOW_OUT'] ] = rep['__FB_POST__']
						
					else:
						logger.debug( 'NON CI sono i VideoDetails ')
						lista_rossi.append( 'id="cell_r' + rowid +  event_map['FB_post'] +'"' )
						lista_rossi.append( 'id="cell_r' + rowid + event_map['live'] + '"' )
						rep['__FB_POST__'] = 'NON CI sono i VideoDetails'
						rep['__LIVE__'] = 'Non Esiste'
						rep['__FB_DASH_URL__'] = '>Non Esiste'
						lista_errori[ valueSoc['livesocialid'] ][ error['WOW_OUT'] ] = rep['__FB_POST__']

					if not 'VideoDetails' in valueUp:

						logger.debug( 'NON CI sono i FBStreamingDetails ')
						lista_rossi.append( 'id="cell_r' + rowid + event_map['wow_out'] + '"' )
						rep['__WOW_OUT__'] = 'Non esiste la entry Wowza'
						lista_rossi.append( 'id="cell_r' + rowid + event_map['FB_post'] + '"' )
						rep['__FB_POST__'] = 'Non esistono i Dettagli del post'
						lista_rossi.append( 'id="cell_r' + rowid + event_map['live'] + '"' )
						rep['__LIVE__'] = 'Non Esiste'
						lista_errori[ valueSoc['livesocialid'] ][ error['WOW_OUT'] ] = rep['__FB_POST__']

					#logger.debug( '---------------------------------\n\n' + str(rep) )

					# inserisco il rep per la lista che finisce nel db
					if '__ECE_ID__' in rep:
						logger.debug( ' Inserisco nella lista lista_id_ece')
						lista_id_ece[ rep['__LIVE_ID_OVER__'] ] = rep
					else:
						logger.debug( ' NON Inserisco nella lista lista_id_ece')

					# use these three lines to do the replacement
					rep = dict((re.escape(k), v) for k, v in rep.iteritems())
					pattern = re.compile("|".join(rep.keys()))
					tmp_template = pattern.sub(lambda m: rep[re.escape(m.group(0))], col_event_template)
					result = result + tmp_template
					

	#logger.debug( result ) 
	lista_tot = { '__LISTA_GIALLI__' :  lista_gialli,'__LISTA_VERDI__' :  lista_verdi,'__LISTA_ROSSI__': lista_rossi, '__LISTA_FB__' : lista_fb, '__LISTA_YT__': lista_yt}
	logger.debug( '------------ END ---------- LiveStreamingMonitor/CreaEventBody ---------------------')
	return [ result , lista_id_ece , lista_tot, lista_errori ]
			

def CreaListaAccese( lista_incoming_streams, lista_stream_targets ):
	logger.debug( '------------ INIT ---------- LiveStreamingMonitor/CreaListaAccese ---------------------')

	# qui arrivano due liste della forma
	# lista_incoming_streams = {u'la1_720p': u'local (Transcoder)', u'la2_1080p': u'local (Transcoder)', u'la1_1080p': u'local (Transcoder)', u'la1': u'wowz://10.72.113.15:64582', u'la2': u'wowz://10.72.113.15:52939', u'la2_720p': u'local (Transcoder)'}
	# e 
	# lista_stream_targets = {u'article:10352105_1497080263748206': {'post_status': u'SCHEDULED_UNPUBLISHED','sourceStreamName': u'enc20a_720p', 'streamName': u'1497080263748206?ds=1&s_sw=0&a=ATjidjFXgOtgnuAE', 'enabled': False}, u'Test_RTMP_FB': {'post_status': u'LIVE','sourceStreamName': u'la1_720p', 'streamName': u'1788762587822119?ds=1&a=AThdLqeIup57TaVR', 'enabled': False}, u'article:10377395_1502781786511387': {'sourceStreamName': u'enc15_720p', 'streamName': u'1502781786511387?ds=1&s_sw=0&a=ATgwZ_BogvZMlgTE', 'enabled': False}}
	# che devono diventare una lista_accese tipo:
	# lista_accese = { mc('la2', 'test_stream' ): { 'colore' : 'rosso' , 'valore' : 'valore_della_cella' }, mc('enc21a', 'sport_post') : { 'colore' : 'giallo' , 'valore' : 'valore_della_cella' }, mc('intrastream', 'rossa_post') : { 'colore' : 'giallo' , 'valore' : 'valore_della_cella' }, 'cell_r03c06' : { 'colore' : 'verde' , 'valore' : 'valore_della_cella' }, 'cell_r04c08' : { 'colore' : 'rosso' , 'valore' : 'valore_della_cella' }, 'cell_r07c05' : { 'colore' : 'verde' , 'valore' : 'CAVE' }, }


	result = {}

	# giro sugli incoming streams
	for  key,value in lista_incoming_streams.iteritems():
		if '_720p' in key or '_1080p' in key:
			continue
		#logger.debug( key,value )
		result[ mc ( key, 'server' ) ] = { 'colore' : 'verde', 'valore' : value }
	
	logger.debug( ' Lista stream Targets : ' )
	logger.debug( lista_stream_targets )
	# poi sugli stram targets
	for  key,value in lista_stream_targets.iteritems():
		if not '_account:' in key:
			continue
		logger.debug( key,value )
		account = 'fb_account_' + key.split( '_account:')[-1]
		# ad account devo aggiungere se si tratta dello stream entrante = stream 
		# come in questo caso oppure della 
		# entry in FB = post
		account_out = account + '_stream'
		logger.debug( account )
		encoder =  value['sourceStreamName']
		encoder =  encoder.replace('_720p','').replace('_1080p','')
		valore_della_cella = value['streamName']
		logger.debug( encoder, valore_della_cella )

		# ultimo il colore se enabled o disabled
		if 'Error' in value['status']:
			colore = 'rosso'
		else:
			if 'Active' in value['status']:
				colore = 'verde'
			else:
				colore = 'giallo'

		result[ mc( encoder, account_out )] = { 'colore' : colore , 'valore' : valore_della_cella }

		account_out = account + '_post'
		#logger.debug( account_out )
		# quindi per i post post
		if 'VOD' in value['post_status']:
			continue
		if 'SCHEDULED_UNPUBLISHED' in value['post_status']:
			logger.debug( '-------------------------- passo di qui' )
			colore = 'giallo'
		else:
			if 'LIVE' in value['post_status']:
				colore = 'verde'
			else:
				if 'ERROR' in value['post_status']:
					colore = 'rosso'

		result[ mc( encoder, account_out )] = { 'colore' : colore , 'valore' : 'valore_da_decidere' }




	logger.debug( result )
	return result
	
def CreaHtml( lista_incoming_streams, lista_stream_targets, lista_active_articles ):

	# qui arrivano due liste della forma
	# lista_incoming_streams = {u'la1_720p': u'local (Transcoder)', u'la2_1080p': u'local (Transcoder)', u'la1_1080p': u'local (Transcoder)', u'la1': u'wowz://10.72.113.15:64582', u'la2': u'wowz://10.72.113.15:52939', u'la2_720p': u'local (Transcoder)'}
	# e 
	# lista_stream_targets = {u'article:10352105_1497080263748206': {'sourceStreamName': u'enc20a_720p', 'streamName': u'1497080263748206?ds=1&s_sw=0&a=ATjidjFXgOtgnuAE', 'enabled': False}, u'Test_RTMP_FB': {'sourceStreamName': u'la1_720p', 'streamName': u'1788762587822119?ds=1&a=AThdLqeIup57TaVR', 'enabled': False}, u'article:10377395_1502781786511387': {'sourceStreamName': u'enc15_720p', 'streamName': u'1502781786511387?ds=1&s_sw=0&a=ATgwZ_BogvZMlgTE', 'enabled': False}}
	# che devono diventare una lista_accese tipo:
	# lista_accese = { mc('la2', 'test_stream' ): { 'colore' : 'rosso' , 'valore' : 'valore_della_cella' }, mc('enc_21a', 'sport_post') : { 'colore' : 'giallo' , 'valore' : 'valore_della_cella' }, mc('intrastream', 'rossa_post') : { 'colore' : 'giallo' , 'valore' : 'valore_della_cella' }, 'cell_r03c06' : { 'colore' : 'verde' , 'valore' : 'valore_della_cella' }, 'cell_r04c08' : { 'colore' : 'rosso' , 'valore' : 'valore_della_cella' }, 'cell_r07c05' : { 'colore' : 'verde' , 'valore' : 'CAVE' }, }

	lista_accese = {}
	lista_accese = CreaListaAccese( lista_incoming_streams, lista_stream_targets ) 
	

	# devo attaccare la prima parte del html
	result_html = html_table_template.table_init

	result_html = result_html + CreaBody( lista_accese )

	# devo attaccare la parte finale del html
	result_html = result_html + html_table_template.table_end
	return [ result_html , lista_accese ]
	
def Cambia_Colori( lista_colori, event_table_init ):


	rep = {}
	for key,value in lista_colori.iteritems():
		style_list = ''
		for val in value:
			style_list = style_list + ' [' + val + '],'

		#print style_list[:-1]

		rep[ key ] = style_list[:-1]

	# use these three lines to do the replacement
	rep = dict((re.escape(k), v) for k, v in rep.iteritems())
	pattern = re.compile("|".join(rep.keys()))
	color_style = pattern.sub(lambda m: rep[re.escape(m.group(0))], html_table_template.event_table_color_style)
	
	rep = {}
	rep['__STILE_COLORI__'] =  color_style

        _time_now = datetime.now()
        rep['__TIME_NOW__'] = _time_now.strftime("%H:%M - %d.%m.%Y")

	# use these three lines to do the replacement
	rep = dict((re.escape(k), v) for k, v in rep.iteritems())
	pattern = re.compile("|".join(rep.keys()))
	tmp_template = pattern.sub(lambda m: rep[re.escape(m.group(0))], event_table_init)
	
	#print tmp_template
	return tmp_template

def CreaEventHtml( lista_incoming_streams, lista_stream_targets, lista_active_articles ):

	# qui arrivano due liste della forma
	# lista_incoming_streams = {u'la1_720p': u'local (Transcoder)', u'la2_1080p': u'local (Transcoder)', u'la1_1080p': u'local (Transcoder)', u'la1': u'wowz://10.72.113.15:64582', u'la2': u'wowz://10.72.113.15:52939', u'la2_720p': u'local (Transcoder)'}
	# e 

	# lista_stream_targets = {u'article:9845290_post:1871897846175259_account:testare_e_bello': {'post_details': {u'status': u'SCHEDULED_UNPUBLISHED', u'is_manual_mode': False, u'description': u'test x dash_preview_url', u'title': u'test x dash_preview_url', u'secure_stream_url': u'rtmps://live-api.facebook.com:443/rtmp/1871897846175259?ds=1&a=ATisYxX0oDuWDAaN', u'live_views': 0, u'creation_time': u'2018-05-23T13:41:40+0000', u'seconds_left': 0, u'dash_preview_url': u'https://video-frt3-2.xx.fbcdn.net/hvideo-ash5/v/reRq6XgCq-kGO-IdR2a4p/live-dash/dash-abr4/1871897846175259.mpd?lvp=1&_nc_rl=AfA7u-NmlPtCm-ni&efg=eyJxZV9ncm91cHMiOnsibGl2ZV9jYWNoZV9wcmltaW5nX3VuaXZlcnNlIjp7ImVuYWJsZWQiOiIxIiwiZm5hX2VuYWJsZWQiOiIwIn19fQ%3D%3D&oh=01011340a01ea94f97a2f5c7d896e675&oe=5B08E8EB', u'broadcast_start_time': u'2018-05-23T13:41:40+0000', u'planned_start_time': u'2018-05-24T14:00:59+0000', u'embed_html': u'<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2F1237091532989230%2Fvideos%2F1871897839508593%2F&width=400" width="400" height="400" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>', u'permalink_url': u'https://www.facebook.com/1237091532989230/videos/1871897839508593/', u'id': u'1871897846175259'}, 'sourceStreamName': u'la2_720p', 'streamName': u'1871897846175259?ds=1&a=ATisYxX0oDuWDAaN', 'enabled': True, 'sessionStatus': u'Active'}}


	# lista_active_articles = {u'10104782': {u'FBStreamingDetails': {}, u'YTStreamingDetails': {}, u'SocialFlags': {'fbVideoDesc': 'TEST DB 2', 'subtitle': None, 'ytVideoDesc': None, 'notificaTime': '2018-05-13T17:30:00Z', 'fbVideoTitle': 'TEST DB 2', 'encoders': ['http://srgssruni14ch-lh.akamaihd.net/z/enc14uni_ch@191858/manifest.f4m', 'https://srgssruni14ch-lh.akamaihd.net/i/enc14uni_ch@191858/master.m3u8', 'https://srgssrrcdvr14ch-lh.akamaihd.net/i/enc14rcdvr_ch@125584/master.m3u8?dw=0', 'http://srgssrrcdvr14ch-lh.akamaihd.net/z/enc14rcdvr_ch@125584/manifest.f4m'], 'title': 'TEST 1 Calcio: Champions League, Siviglia - Manchester United', 'ytVideoTitle': 'TEST 1 Calcio: Champions League, Siviglia - Manchester United', 'incoming_stream': 'enc14', 'state': 'published', 'setNotifTime': True, 'Geo': True, 'socialEndTime': '2018-05-13T21:00:00Z', 'fb-livestreaming-account': ['testare_e_bello'], 'yt-livestreaming-account': [], 'activation': None, 't_start': '2018-05-13T20:39:59Z', 'continuous_live': False, 'startTime': '2018-05-13T18:40:00Z', 'socialStartTime': '2018-05-13T18:40:00Z', 't_end': '2018-05-13T22:59:59Z', 't_notifica': '2018-05-13T19:29:59Z', 'expiration': None, 'published': '2018-02-21T19:28:00.000Z', 'endTime': '2018-05-13T21:00:00Z'}, u'ECEDetails': {u'contenttype': u'livestreaming', u'title': u"TEST_local time piu' un minuto PROD 09.02.2018", u'lastmodifieddate': u'2018-02-27T11:08:00Z', u'publishdate': u'2018-02-03T17:00:00Z', u'state': u'published', u'creationdate': u'2017-11-30T13:00:00Z', u'id': u'article:9845290'}}, u'10104779': {u'FBStreamingDetails': {}, u'YTStreamingDetails': {}, u'SocialFlags': {'fbVideoDesc': 'TEST DB', 'subtitle': None, 'ytVideoDesc': None, 'notificaTime': '2018-05-12T17:30:00Z', 'fbVideoTitle': 'TEST db', 'encoders': ['http://srgssruni13ch-lh.akamaihd.net/z/enc13uni_ch@191855/manifest.f4m', 'https://srgssruni13ch-lh.akamaihd.net/i/enc13uni_ch@191855/master.m3u8', 'https://srgssrrcdvr14ch-lh.akamaihd.net/i/enc14rcdvr_ch@125584/master.m3u8?dw=0', 'http://srgssrrcdvr14ch-lh.akamaihd.net/z/enc14rcdvr_ch@125584/manifest.f4m'], 'title': 'Calcio: Champions League, Chelsea - Barcellona', 'ytVideoTitle': 'Calcio: Champions League, Chelsea - Barcellona', 'incoming_stream': 'la2', 'state': 'published', 'setNotifTime': True, 'Geo': True, 'socialEndTime': '2018-05-12T21:15:00Z', 'fb-livestreaming-account': ['testare_e_bello'], 'yt-livestreaming-account': [], 'activation': None, 't_start': '2018-05-12T20:29:59Z', 'continuous_live': True, 'startTime': '2018-05-12T18:30:00Z', 'socialStartTime': '2018-05-12T18:30:00Z', 't_end': '2018-05-12T23:14:59Z', 't_notifica': '2018-05-12T19:29:59Z', 'expiration': None, 'published': '2018-02-20T19:28:00.000Z', 'endTime': '2018-05-12T21:15:00Z'}, u'ECEDetails': {u'contenttype': u'livestreaming', u'title': u"TEST_local time piu' un minuto PROD 09.02.2018", u'lastmodifieddate': u'2018-02-27T11:08:00Z', u'publishdate': u'2018-02-03T17:00:00Z', u'state': u'published', u'creationdate': u'2017-11-30T13:00:00Z', u'id': u'article:9845290'}}}
	# che devono diventare una lista_accese tipo:
	# lista_accese = { mc('la2', 'test_stream' ): { 'colore' : 'rosso' , 'valore' : 'valore_della_cella' }, mc('enc_21a', 'sport_post') : { 'colore' : 'giallo' , 'valore' : 'valore_della_cella' }, mc('intrastream', 'rossa_post') : { 'colore' : 'giallo' , 'valore' : 'valore_della_cella' }, 'cell_r03c06' : { 'colore' : 'verde' , 'valore' : 'valore_della_cella' }, 'cell_r04c08' : { 'colore' : 'rosso' , 'valore' : 'valore_della_cella' }, 'cell_r07c05' : { 'colore' : 'verde' , 'valore' : 'CAVE' }, }


	# faccio il sorting del dict per valore t_start
	sorted_active_articles = sorted(lista_active_articles.items(), key=lambda (k,v) : datetime.strptime(v['SocialFlags']['t_start'],'%Y-%m-%dT%H:%M:%SZ' ),reverse=False)


	# devo attaccare la prima parte del html
	[ middle_html, lista_id_ece , lista_colori , lista_errori ] = CreaEventBody( lista_incoming_streams, lista_stream_targets, sorted_active_articles)

	logger.debug( 'lista_colori da CreaEventBody : ' + str(lista_colori ))
	result_html = Cambia_Colori( lista_colori, html_table_template.event_table_init )
	result_html = result_html + middle_html + html_table_template.event_table_end

	# devo attaccare la parte finale del html
	return [ result_html , lista_id_ece , lista_errori ]
			

if __name__ == "__main__":

	lista_accese = { 
		mc('la2', 'test_stream' ): { 'colore' : 'rosso' , 'valore' : 'valore_della_cella' },
		mc('enc_21a', 'sport_post') : { 'colore' : 'giallo' , 'valore' : 'valore_della_cella' },
		mc('intrastream', 'rossa_post') : { 'colore' : 'giallo' , 'valore' : 'valore_della_cella' },
		'cell_r03c06' : { 'colore' : 'verde' , 'valore' : 'valore_della_cella' },
		'cell_r04c08' : { 'colore' : 'rosso' , 'valore' : 'valore_della_cella' },
		'cell_r07c05' : { 'colore' : 'verde' , 'valore' : 'CAVE' },
			}
	lista_accese = {}
	body_html = CreaEventBody( lista_accese )
	print body_html

